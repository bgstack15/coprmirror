# Readme for coprmirror
`coprmirror` is a yum mirror solution, with a wrapper to mirror specifically a named [COPR](https://copr.fedorainfracloud.org/coprs/) repository.

## Overview
COPR is a distro-run community offering that lets users build yum/dnf repositories with their own software (primarily if not exclusively in rpm format). COPR performs the builds, and then hosts the binary and source rpms for client machines. This project downloads using native GNU/Linux tools the yum repositories that collectively make up a COPR, so each release-releasever-basearch triplet. The end goal is to have a local copy of the entire current yum repos. This does not copy the build assets or log files; just what a yum repository defines and the gpg public key.

Notably this utility needs the yum python packages present only for evaluating yum variables in the inurl (baseurl from a .repo file), so if you have a string literal as the inurl value, you can run this on a system that does not have yum installed.

## Using
Configure coprmirror.conf from the provided .example file, and then run:

    COPRMIRROR_CONF=coprmirror.conf VERBOSE=1 DEBUG=1 ./coprmirror.sh

## Upstream
Original content
The `get_file` function was improved after being imported from my [Reference 1](https://gitlab.com/bgstack15/former-gists/-/blob/master/obsmirror.sh/obsmirror.sh) script.

## Alternatives
1. I felt like ansible and system are overkill, but if you like those, this is perfect for you:
   [https://github.com/ganto/ansible-copr_reposync](https://github.com/ganto/ansible-copr_reposync)

## Dependencies
wget, grep, awk, sed, jq

## References
1. [obsmirror](https://gitlab.com/bgstack15/former-gists/-/blob/master/obsmirror.sh/obsmirror.sh)
2. [Mirror an OBS repository locally — update 1](https://bgstack15.wordpress.com/2020/03/11/mirror-an-obs-repository-locally-update-1/)

## Differences from upstream
N/A
